[[_TOC_]]

# [vaultwarden](https://github.com/dani-garcia/vaultwarden) Packages

## What it is

* apt repo with Ubuntu packages (systemd)
* supports all database backends
  * SQlite
  * MySQL
  * PostgreSQL
* binary extracted from pre-build docker images + [web-vault](https://github.com/dani-garcia/bw_web_builds/releases)

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-vaultwarden.asc https://packaging.gitlab.io/vaultwarden/gpg.key
```

## Add repo to apt

### Stable

```bash
echo "deb https://packaging.gitlab.io/vaultwarden/vaultwarden vaultwarden stable" | sudo tee /etc/apt/sources.list.d/morph027-vaultwarden-stable.list
```

### Testing

```bash
echo "deb https://packaging.gitlab.io/vaultwarden/vaultwarden vaultwarden testing" | sudo tee /etc/apt/sources.list.d/morph027-vaultwarden-testing.list
cat >/etc/apt/preferences.d/vaultwarden-testing <<EOF
Package: *
Pin: release o=vaultwarden,a=vaultwarden,c=testing
Pin-Priority: 10
EOF
```

To install from testing: `apt-get -t c=testing install vaultwarden`


## Automatic updates using unattended-upgrades

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"vaultwarden:vaultwarden";};' | sudo tee /etc/apt/apt.conf.d/50vaultwarden
```

## Configuration

* create `/etc/vaultwarden/user.env` with settings you like (see [template](https://github.com/dani-garcia/vaultwarden/blob/main/.env.template))

## Systemd Tweaks

If you like to overwrite package systemd instructions, you can use [systemd dropins](https://github.com/coreos/docs/blob/master/os/using-systemd-drop-in-units.md).

### Example: wait for postgresl on localhost

```bash
mkdir -p /etc/systemd/system/vaultwarden.service.d/
cat > /etc/systemd/system/vaultwarden.service.d/10-postgresql.conf << EOF
[Unit]
After=network.target postgresql.service
Requires=postgresql.service
EOF
```
