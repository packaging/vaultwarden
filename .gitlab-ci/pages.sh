#!/bin/bash

set -e

dist=vaultwarden

mkdir -p "${CI_PROJECT_DIR}"/public/"${dist}"
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/deb/"${dist}"/"${dist}"/{pool,dists} \
  "${CI_PROJECT_DIR}"/public/"${dist}"/
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/deb/gpg.key \
  "${CI_PROJECT_DIR}"/public/
