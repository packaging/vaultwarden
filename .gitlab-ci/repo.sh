#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

dist=vaultwarden

for dir in "${CI_PROJECT_DIR}"/package-*; do
    export SCAN_DIR="${dir}"
    export COMPONENT="stable"
    if [[ "${dir}" =~ ^.*package-.*-testing$ ]]; then
        export COMPONENT="testing"
    fi
    /deb.sh "${dist}"
done;
