#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-0.0.0+1}"
version="${tag%%+*}"
patchlevel="${tag##*+}"
patchlevel="${patchlevel:-${patchlevel}}"
name=vaultwarden
description="Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs"
architecture="${ARCH:-amd64}"
web_vault="${WEB_VAULT:-v2025.1.1}"
if [[ "${version}" =~ ^@.*$ ]]; then
    vaultwarden_branch="${version/@/}"
fi

. /etc/lsb-release

apt-get -qq update
apt-get -qqy install \
    binutils \
    curl \
    docker.io \
    file \
    ruby-dev \
    ruby-ffi

type fpm >/dev/null 2>&1 || (
    gem install dotenv -v 2.8.1 --no-doc
    gem install fpm --no-doc
)

tmpdir="$(mktemp -d)"
mkdir -p "${tmpdir}/usr/bin" "${tmpdir}/usr/share/vaultwarden" "${tmpdir}/etc/vaultwarden"

export DOCKER_DEFAULT_PLATFORM="linux/${architecture}"
docker create --name vaultwarden vaultwarden/server:"${version}"
docker cp vaultwarden:/vaultwarden "${tmpdir}/usr/bin/"
docker rm -f vaultwarden
file --mime-type --brief "${tmpdir}/usr/bin/vaultwarden" | grep -q 'application/x-pie-executable'
cp -v "${CI_PROJECT_DIR}/.packaging/default.env" "${tmpdir}/etc/vaultwarden/"
if [ ! -f /tmp/web-vault.tgz ]; then
    curl -sLo /tmp/web-vault.tgz "https://github.com/dani-garcia/bw_web_builds/releases/download/${web_vault}/bw_web_${web_vault}.tar.gz"
fi
tar -C "${tmpdir}/usr/share/vaultwarden" -xzf /tmp/web-vault.tgz

case "${DISTRIB_CODENAME}" in
*)
    libssl="libssl3"
    ;;
esac

cat >"${tmpdir}/changelog" <<EOF
vaultwarden (${CI_COMMIT_TAG}) ${DISTRIB_CODENAME}; urgency=medium

  * https://github.com/dani-garcia/vaultwarden/releases/tag/${version}

 -- Stefan Heitmüller <stefan.heitmueller@gmx.com> $(date "+%a, %d %b %Y %H:%m:%S %z")
EOF
if [ -n "${vaultwarden_branch}" ]; then
    tags="$(git -C /tmp/vaultwarden describe --tags --dirty)"
    epoch=0
    version="${tags%%-*}"
    patchlevel="${patchlevel}+${vaultwarden_branch}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}-testing"
else
    version="${version//[a-zA-Z]/}"
    epoch="${version%%.*}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}"
fi
mkdir -p "${output}"
fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${name}" \
    --package "${output}/${name}_${version}+${patchlevel}_${architecture}.deb" \
    --deb-changelog "${tmpdir}/changelog" \
    --architecture "${architecture}" \
    --version "${version}+${patchlevel}" \
    --license AGPL-3.0 \
    --vendor dani-garcia \
    --epoch "${epoch}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://github.com/dani-garcia/vaultwarden/" \
    --description "${description}" \
    --depends libmariadb3 \
    --depends libpq5 \
    --depends "${libssl}" \
    --depends systemd \
    --deb-recommends morph027-keyring \
    --prefix "/" \
    --chdir "${tmpdir}" \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    --deb-systemd "${CI_PROJECT_DIR}/.packaging/vaultwarden.service" \
    etc usr
