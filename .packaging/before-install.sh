getent passwd vaultwarden >/dev/null 2>&1 || adduser \
  --system \
  --shell /usr/sbin/nologin \
  --gecos 'vaultwarden' \
  --group \
  --disabled-password \
  --home /var/lib/vaultwarden \
  vaultwarden
